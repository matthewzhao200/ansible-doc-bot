# bot.py
import os
import random
import yaml
import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()


file = open('urls.yml')
documents = yaml.full_load(file)


bot = commands.Bot(command_prefix='?')

@bot.command(name='doc')
async def nine_nine(ctx, arg):
        answer = 0
        embedVar = discord.Embed(title="Docs", description="ansible documentation realated to " + arg, color=0x00ff00)
        for link in documents:
                        if arg in link['name'] and answer < 10: 
                               embedVar.add_field(name=link['name'], value=link['url'] , inline=False)
                               answer = answer +1
        await ctx.send(embed=embedVar)

bot.run(TOKEN)
