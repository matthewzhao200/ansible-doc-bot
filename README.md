# Discord-ansible-doc
> A Discord bot to read ansible documentation

## Setup

1. Creat a discord bot 
  - see https://discordpy.readthedocs.io/en/latest/discord.html
2. Install bot server software
  - clone the repository `git clone https://gitlab.com/awol-linux/ansible-doc-bot.git`
  - Install requirments `pip install -r requirments.yml`
  - create a .env file and put `DISCORD_TOKEN={your-bot-token}` inside it
  - run `python3 ./scrape_urls.py > urls.yml` in order to creat the module list
  - start the bot `python3 ./bot.py

3. Once the bot is added to your server, you can interact with it using the commands listed below.

## Usage

Commands for this bot follow this structure: `?<command>`.

| Command | Description
|---------|-------------|
| `?doc <search term>` | Will search through the module list and respond with a link. |
| `?doc help` | Displays usage instructions (not yet). |

